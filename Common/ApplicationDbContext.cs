﻿using ConnectToDb.Models;
using MySql.Data.MySqlClient;
using System.Diagnostics.Metrics;
using System.Xml.Linq;

namespace ConnectToDb.Common
{
    public class ApplicationDbContext
    {
        public static string ConnectionStringSettings = "server=localhost;port=3308;user=root;password=12345678;database=test;Persist Security Info=False; Convert Zero Datetime=True;Allow Zero Datetime=True;Connect Timeout=300";

        public static Customers SaveCustomer(Customers customers)
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionStringSettings))
            {
                using (MySqlCommand cmd = new MySqlCommand("INSERT INTO Customers (Name, Country) VALUES (@Name, @Country)"))
                {
                    using (MySqlDataAdapter sda = new MySqlDataAdapter())
                    {
                        cmd.Parameters.AddWithValue("@Name", customers.Name);
                        cmd.Parameters.AddWithValue("@Country", customers.Country);
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            return customers;
        }
    }
}
