﻿using ConnectToDb.Common;
using ConnectToDb.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace ConnectToDb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            Customers c = new Customers();
            c.Name = "kanchan";
            c.Country = "India";
            c = ApplicationDbContext.SaveCustomer(c);
            Console.WriteLine(c.Name, c.Country, "Successfully");
            Console.WriteLine("Index Controller Called .");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}