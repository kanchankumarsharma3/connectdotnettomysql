﻿namespace ConnectToDb.Models
{
    public class Seller
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShopName { get; set; }
        public DateTime opens { get; set; }
        public DateTime closed { get; set; }
    }
}
